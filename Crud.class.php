<?php
include_once "Conexion.class.php";
$pdo= new Conexion();

class Crud
{
    public $sql;
    
    public function selectRows()
    { 
        try{
            global $pdo;
            $stmt = $pdo->dbh->prepare($this->sql);
            $stmt->setFetchMode(PDO::FETCH_ASSOC); 
            $stmt->execute();
            $rows = $stmt->fetchAll();
            return $rows;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function insert($values) 
    {
        try{
            global $pdo; 
            $stmt= $pdo->$dbh->prepare($this->sql); 
            $stmt->execute($values); 
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function updateByID($values)
    {
        try{
            global $pdo; 
            $stmt= $pdo->dbh->prepare($this->sql); 
            $stmt->execute($values);
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function deleteByID($id) 
    {
        try{
            global $pdo; 
            $stmt= $pdo->dbh->prepare($this->sql); 
            $stmt->execute($id); 
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function getByID($id)
    {
        try{
            global $pdo;
            $stmt= $pdo->dbh->prepare($this->sql);        
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute($id);
            $rows = $stmt->fetchAll();
            return $rows;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}

//$alumno=new Alumno();
/*$alumno->codigo=55; //**********registro a editar**********
$alumno->nombre="Jonatan Armando Flores García";
$alumno->tipoDocumento=1;
$alumno->numeroDocumento="02356393-8";
$alumno->sexo="Masculino";
$alumno->correo="jflores@gmail.com";
$alumno->telefono="7589-6354";
$alumno->direccion="Usulután";*/

/*
$alumno=new Alumno();

$alumno->codigo=55;

$alumno->delete();

echo "<pre>";
print_r($alumno->getAll());
echo "</pre>"; 

//$al->codigo=1;
//$al->nombre="PRUEBA ----------------------";
//print_r($al->getAll());
//print_r($al->save());

//$al->update();

print_r($alumno->getAll());*/