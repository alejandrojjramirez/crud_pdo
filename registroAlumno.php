<?php
  include_once "Conexion.class.php";
?>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Registro de Alumnos</title>
<style type="text/css">
@import url("css/mycss.css");
</style>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
	</head>
	<body>
	<div class="todo">
  
  <div id="cabecera">
  	<img src="images/swirl.png" width="1188" id="img1">
  </div>
  
  <div id="contenido">
  	<div style="margin: auto; width: 800px; border-collapse: separate; border-spacing: 10px 5px;">
  		<span> <h1>Registro de Alumnos</h1> </span>
  		<br>
	  <form action="registro.php" method="POST" style="border-collapse: separate; border-spacing: 10px 5px;">
  		<label>Nombre: </label>
  		<input type="text" name="nombre" /><br/>
  		
  		<label>Tipo Documento </label>
  		<select name="tipoDocumento" id="tipo" onchange="respuesta()">
				<option value="1">DUI</option>
				<option value="2">Pasaporte</option>
			</select><br/>

		<label>Numero de Documento: </label>
		<input type="text" name="numDocumento" id="numDocumento" placeholder="000000000-0"/><br/>
  		
		<label>Sexo: </label> 
		<select name="sexo">
				<option value="Masculino">Masculino</option>
				<option value="Femenino">Femenino</option>
			</select><br/>

		<label>Correo: </label>
		<input type="email" name="correo"/><br/>

		<label>Telefono: </label>
		<input type="text" name="telefono"/><br/>

		<label>Direccion: </label>
  		<textarea style="border-radius: 10px;" rows="3" cols="50" name="descripcion" ></textarea><br>
  		
  		<br>
  		<button type="submit" class="btn btn-success">Guardar</button>
     </form>
  	</div>
  	
  </div>
  
	<div id="footer">
  		<img src="images/swirl2.png" id="img2">
  	</div>

</div>
<script type="text/javascript">
			function respuesta(){			   
			   var txt =  document.getElementById('numDocumento');
			   var val = document.getElementById('tipo');
			   
			   if(val.value=="1"){				
                  // alert("DUI");			
					var placeholder ="000000000-0";
					txt.placeholder= placeholder;
			   }else if(val.value == "2"){
                   //alert("pasaporte");
				  var placeholder ="A0000000";
				  txt.placeholder= placeholder;
			   }
			   
			}
		
		</script>

</body>
</html>