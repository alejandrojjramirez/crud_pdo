<?php
include_once "Conexion.class.php";

class Alumno
{
    //atributos 
    private $codigo;
    private $nombre;
    private $tipoDocumento;
    private $numeroDocumento;
    private $sexo;
    private $correo;
    private $telefono;
    private $direccion;

    private $con;

    public function construct(){
        $this->con = new Conexion();
    }

    public function set($atributo, $contenido){
        $this->atributo = $contenido;
    }

    public function get($atirbuto)
    {
        $this->atributo;
    }
    public function crear(){
        $sql2 = "SELECT * FROM alumno WHERE codigo = '{$this->codigo}'";
        $resultado = $this->con->consultaRetorno($sql2);
        $num = mysql_num_rows($resultado);

        if($num != 0 ){
            return false;
        }else{
            $sql = "INSERT INTO alumno(codigo, nombreCompleto, tipoDocumento, numeroDocumento,sexo, correo,numeroTelefono,direccion) VALUES (
                '{$this->nombreCompleto}', '{$this->tipoDocumento}', '{$this->numeroDocumento}', '{$this->sexo}', '{$this->correo}', '{$this->numeroTelefono}', '{$this->direccion}', NOW())";
                $this->consultaSimple($sql);
        }
        
    }
    
    public function guardar()
    {
        global $crud; 
        $crud->sql="INSERT INTO alumno(codigo, nombreCompleto, tipoDocumento, numeroDocumento,sexo, correo,numeroTelefono,direccion) 
        VALUES (:nombreCompleto,:tipoDocumento, :numeroDocumento, :sexo, :correo, :numeroTelefono, :direccion)"; 

        $vals= array(
            ":nombreCompleto"=>$this->nombre,
            ":tipoDocumento"=>$this->tipoDocumento,
            ":numeroDocumento"=>$this->numeroDocumento,
            ":sexo"=>$this->sexo,
            ":correo"=>$this->correo,
            ":numeroTelefono"=>$this->telefono,
            ":direccion"=>$this->direccion
        );
        $crud->insert($vals); 
    }

    public function modificar()
    {
        $sql = "UPDATE alumno SET nombreCompleto = '{$this->nombreCompleto}', correo = '{$this->correo}', numeroTelefono = '{$this->numeroTelefono}', 
        direccion = '{$this->direccion}' WHERE codigo = '{$this->codigo}'";
        $this->con->consultaSimple($sql);
    }

    public function eliminar()
    {
        $sql= "DELETE FROM alumno WHERE codigo = '($this->codigo)'";
        $this->con->consultSimple($sql); 
    }
}



